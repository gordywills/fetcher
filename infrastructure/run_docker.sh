#!/usr/bin/env bash

docker pull gordywills/fetcher:latest
docker run \
    -v /var/fetcher/app.config.ini:/app/app.config.ini \
    -v /var/fetcher/application.log:/app/application.log \
    -v /var/fetcher/fetcher.db:/app/fetcher.db \
    gordywills/fetcher:latest