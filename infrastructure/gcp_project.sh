#!/usr/bin/env bash

setup_project() {
  PROJECT_IDS=$(gcloud projects list --filter="NAME~${PROJECT_NAME}" --format="value(project_id)")
  if [[ -n ${PROJECT_IDS} ]]; then
    echo "Project exists - Skipping create step"
    PROJECT_ID=$(echo ${PROJECT_IDS} | head -n 1)
  else
    UUID_SUFFIX=$(uuidgen | cut -c1-6)
    PROJECT_ID=${PROJECT_NAME}-${UUID_SUFFIX}
    echo "creating Project ${PROJECT_ID}..."
    gcloud projects create ${PROJECT_ID}
    echo "Associating billing account..."
    gcloud beta billing projects link ${PROJECT_ID} --billing-account=${BILLING_ACCOUNT}
  fi
  echo "setting default project..."
  gcloud config set core/project ${PROJECT_ID}
  echo "enabling functions..."
  gcloud services enable cloudfunctions.googleapis.com --project=${PROJECT_ID}
  echo "enabling scheds..."
  gcloud services enable cloudscheduler.googleapis.com --project=${PROJECT_ID}
  echo "enabling compute..."
  gcloud services enable compute.googleapis.com --project=${PROJECT_ID}
  echo "enabling App Engine..."
  gcloud services enable appengine.googleapis.com --project=${PROJECT_ID}
  echo "enabling Pub/Sub..."
  gcloud services enable pubsub.googleapis.com --project=${PROJECT_ID}

}

deploy_cloud_functions() {
  DEPLOYED_START_HASH=$(gcloud functions list --project="${PROJECT_ID}" --filter="NAME~startInstance" --format="value(labels)")
  DEPLOYED_STOP_HASH=$(gcloud functions list --project="${PROJECT_ID}" --filter="NAME~stopInstance" --format="value(labels)")
  CODE_START_HASH=$(sha1sum ./startInstance/* | sha1sum | cut -f1 -d" ")
  CODE_STOP_HASH=$(sha1sum ./stopInstance/* | sha1sum | cut -f1 -d" ")
  if ! [[ ${DEPLOYED_START_HASH} =~ ${CODE_START_HASH} ]]; then
    echo "deploying startInstance could func to ${REGION}..."
    gcloud functions deploy startInstance --region="${REGION}" --project="${PROJECT_ID}" --entry-point="start_instance" --runtime="python37" --source="./startInstance/" --update-labels="hash=${CODE_START_HASH}" --trigger-topic="start"
  else
    echo "skipping startInstance function deploy as there are no changes..."
  fi
  if ! [[ ${DEPLOYED_STOP_HASH} =~ ${CODE_STOP_HASH} ]]; then
    echo "deploying stopInstance could func to ${REGION}..."
    gcloud functions deploy stopInstance --region="${REGION}" --project="${PROJECT_ID}" --entry-point="stop_instance" --runtime="python37" --source="./stopInstance/" --update-labels="hash=${CODE_STOP_HASH}" --trigger-topic="stop"
  else
    echo "skipping stopInstance function deploy as there are no changes..."
  fi
}

setup_scheduler() {
  echo "setting up Sched Jobs..."
  if gcloud app describe; then
    echo "skipping app create..."
  else
    echo "creating app..."
    gcloud app create --region=${REGION} --project=${PROJECT_ID}
  fi
  JOBS_LIST=$(gcloud scheduler jobs list --format="value(name)")
  START_COMMAND="create"
  STOP_COMMAND="create"
  for JOB in ${JOBS_LIST[@]}; do
    case ${JOB} in
    "startJob")
      START_COMMAND="update"
      ;;
    "stopJob")
      STOP_COMMAND="update"
      ;;
    esac
  done
  echo "setting up Start Job..."
  DEPLOYED_START_HASH=$(gcloud scheduler jobs list --filter="NAME~startJob" --format="value(description)")
  CODE_START_HASH=$(echo ${START_SCHEDULE} | sha1sum | cut -f1 -d" ")
  if ! [[ ${DEPLOYED_START_HASH} =~ ${CODE_START_HASH} ]]; then
    gcloud scheduler jobs ${START_COMMAND} pubsub startJob --project="${PROJECT_ID}" --description=${CODE_START_HASH} --schedule="${START_SCHEDULE}" --topic="start" --message-body="{\"project\": \"${PROJECT_ID}\",\"zone\": \"${ZONE}\",\"instance\": \"${INSTANCE_NAME}\"}"
  fi
  echo "setting up Stop Job..."
  DEPLOYED_STOP_HASH=$(gcloud scheduler jobs list --filter="NAME~stopJob" --format="value(description)")
  CODE_STOP_HASH=$(echo ${STOP_SCHEDULE} | sha1sum | cut -f1 -d" ")
  if ! [[ ${DEPLOYED_STOP_HASH} =~ ${CODE_STOP_HASH} ]]; then
    gcloud scheduler jobs ${STOP_COMMAND} pubsub stopJob --project="${PROJECT_ID}" --description=${CODE_STOP_HASH} --schedule="${STOP_SCHEDULE}" --topic="stop" --message-body="{\"project\": \"${PROJECT_ID}\",\"zone\": \"${ZONE}\",\"instance\": \"${INSTANCE_NAME}\"}"
  fi
}

self_update() {
  gcloud components update
  gcloud config set core/disable_prompts false
  gcloud config set core/disable_usage_reporting false
}

self_auth() {
  if [[ -n ${KEY_FILE} ]]; then
    echo ${KEY_FILE} | base64 -d >/tmp/key-file.json
    gcloud auth activate-service-account--key-file /tmp/key-file.json
    rm /tmp/key-file.json
    gcloud config set compute/region ${DEFAULT_REGION}
    gcloud config set compute/zone ${DEFAULT_ZONE}
  else
    if [[ ${CI} == "true" ]]; then
      echo "Cannot be in CI mode without KEY_FILE env var. Exciting as have no credentials."
      exit 1
    fi
  fi
}

check_ini_file() {
  if [[ ! -f "../app.config.ini" ]]; then
    if [[ -n ${INI_FILE} ]]; then
      echo ${INI_FILE} | base64 -d >../app.config.ini
    else
      echo "INI_FILE does not exists and no env var is available to use.  Exiting."
      exit 1
    fi
  fi
}

setup_compute() {
  echo "setting up compute instances..."
  INSTANCE_LIST=$(gcloud compute instances list --format="value(name)")
  FOUND="false"
  for INSTANCE in ${INSTANCE_LIST[@]}; do
    if [[ ${INSTANCE} == ${INSTANCE_NAME} ]]; then
      FOUND="true"
    fi
  done
  if [[ "${FOUND}" == "false" ]]; then
    echo "creating compute instance..."
    gcloud compute instances create-with-container ${INSTANCE_NAME} --project=${PROJECT_ID} --zone=${ZONE} --machine-type=f1-micro --subnet=default --network-tier=PREMIUM --metadata=google-logging-enabled=true --no-restart-on-failure --image=cos-stable-75-12105-97-0 --image-project=cos-cloud --boot-disk-size=10GB --boot-disk-type=pd-standard --container-image=${IMAGE_NAME} --container-restart-policy=never --container-privileged --labels=container-vm=cos-stable-75-12105-97-0
    gcloud compute ssh ${INSTANCE_NAME} --command="sudo mkdir -p /var/fetcher/"
    gcloud compute ssh ${INSTANCE_NAME} --command="sudo chmod 777 /var/fetcher/"
    gcloud compute ssh ${INSTANCE_NAME} --command="touch /var/fetcher/application.log"
    gcloud compute ssh ${INSTANCE_NAME} --command="touch /var/fetcher/fetcher.db"
    gcloud compute scp ../app.config.ini ${INSTANCE_NAME}:/var/fetcher/app.config.ini
    gcloud compute instances update-container ${INSTANCE_NAME} --project=${PROJECT_ID} --zone=${ZONE} --container-mount-host-path=mount-path=/app/app.config.ini,host-path=/var/fetcher/app.config.ini,mode=rw --container-mount-host-path=mount-path=/app/application.log,host-path=/var/fetcher/application.log,mode=rw --container-mount-host-path=mount-path=/app/fetcher.db,host-path=/var/fetcher/fetcher.db,mode=rw
  else
    gcloud compute scp ../app.config.ini ${INSTANCE_NAME}:/var/fetcher/app.config.ini
    echo "skipping compute create step..."
  fi
}

PROJECT_NAME=fetcher

START_SCHEDULE="0 * * * *"
STOP_SCHEDULE="5 * * * *"
INSTANCE_NAME="fetcher-instance"
IMAGE_NAME="gordywills/fetcher"

DEFAULT_REGION="europe-west2"
DEFAULT_ZONE="europe-west2-c"
INI_FILE_PATH="app.config.ini"

self_update
self_auth
check_ini_file
BILLING_ACCOUNT=$(gcloud beta billing accounts list --format="value(name)")
REGION=$(gcloud config get-value compute/region)
ZONE=$(gcloud config get-value compute/zone)
setup_project
deploy_cloud_functions
setup_scheduler
setup_compute

echo ${PROJECT_ID}
