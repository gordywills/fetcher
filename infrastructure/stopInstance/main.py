from googleapiclient import discovery
import base64
import json


def stop_instance(event, context):
    message = json.loads(base64.b64decode(event['data']).decode('utf-8'))
    service = discovery.build('compute', 'v1')
    project = message['project']
    zone = message['zone']
    instance = message['instance']
    request = service.instances().stop(project=project, zone=zone, instance=instance)
    return request.execute()