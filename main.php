<?php

use Fetcher\Application;

require_once "vendor" . DIRECTORY_SEPARATOR . "autoload.php";

$app         = new Application();
$allMessages = $app->fetchAllMessages();
if ($argv[1] == "test") {
    echo "exiting as is test\n";
    print_r("number of messages collected: " . count($allMessages) . "\n");
    exit();
}
$app->sendAllMessages($allMessages);
