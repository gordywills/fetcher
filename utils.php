<?php
require_once "vendor" . DIRECTORY_SEPARATOR . "autoload.php";

use Fetcher\Logger\LoggerError;
use Fetcher\Logger\LoggerFactory;
use Monolog\Logger;
use vierbergenlars\Semver\SemVerException;
use vierbergenlars\SemVer\version;

switch ($argv[1]) {
    case 'version':
        version();
        break;
    case 'patch':
        increment("patch");
        break;
    case 'minor':
        increment("minor");
        break;
    case 'major':
        increment("major");
        break;
    default:
        break;
}

function version()
{
    $logger      = setupLogger();
    $lastTag     = exec('git describe --abbrev=0');
    $versionFile = file_get_contents("version.txt");
    try {
        $tagVersion  = new version($lastTag);
        $fileVersion = new version($versionFile);
    } catch (SemVerException $e) {
        echo $e->getMessage();
        exit();
    }
    $logger->info("Tag Version:", [$tagVersion->getVersion()]);
    $logger->info("File Version:", [$fileVersion->getVersion()]);
    if (version::gt($fileVersion, $tagVersion)) {
        $logger->info("Tagging local repo with: ", [$fileVersion->getVersion()]);
        exec("git tag -a \"v"
             . $fileVersion->getVersion()
             . "\" -m \"Automated Tag from pipeline, based on change in version.txt\"");
        $logger->info("Pushing Tags");
        exec("git push origin --tags");
    } elseif (version::lt($fileVersion, $tagVersion)) {
        throw new UnexpectedValueException("version file lower than last tag in repo");
    }
}

function setupLogger(): Logger
{
    try {
        $logger = LoggerFactory::factory(
            new Monolog\Handler\StreamHandler(
                "php://stdout",
                Logger::INFO
            ),
            "console"
        );
    } catch (\InvalidArgumentException $e) {
        throw new LoggerError("Error setting up logs", 0, $e);
    } catch (\Exception $e) {
        throw new LoggerError("Unable to create the log", 0, $e);
    }

    return $logger;
}

function increment(string $what)
{
    $logger      = setupLogger();
    $versionFile = file_get_contents("version.txt");
    try {
        $fileVersion = new version($versionFile);
    } catch (SemVerException $e) {
        echo $e->getMessage();
        exit();
    }
    $logger->info("updating " . $what . " from: ", [$fileVersion->getVersion()]);
    $fileVersion->inc($what);
    $logger->info("Setting new version to: ", ["v" . $fileVersion->getVersion()]);
    file_put_contents("version.txt", "v" . $fileVersion->getVersion());
}
