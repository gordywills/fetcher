FROM gordywills/php7.3.6-xdebug:latest

WORKDIR /app

ADD . /app

RUN composer -n self-update
RUN composer -n install --no-dev

ENTRYPOINT ["php", "main.php"]
