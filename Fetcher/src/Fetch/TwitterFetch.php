<?php

namespace Fetcher\Fetch;

use Abraham\TwitterOAuth\TwitterOAuth;
use DateTimeImmutable;
use Fetcher\Application;
use Fetcher\Message\Message;
use Fetcher\Message\Messages;
use stdClass;

class TwitterFetch implements FetchInterface
{
    use FetchTrait;

    const LAST_TWEET_ID = "lastTweetID";
    private $connection;
    private $config;
    private $app;
    private $displayName = "twitter";

    public function __construct(Application $app, string $displayName, TwitterOAuth $twitter_conn, array $config)
    {
        $this->app         = $app;
        $this->connection  = $twitter_conn;
        $this->config      = $config;
        $this->displayName = $displayName;
    }

    public function fetchMessages(): Messages
    {
        $this->app->logger->debug($this->displayName . " Getting Messages");
        $user        = $this->getUserInfo();
        $lastTweetID = $this->app->db->getValue($this->displayName . " " . $this::LAST_TWEET_ID);
        $this->app->logger->debug($this->displayName . " last known tweet ID", [$lastTweetID]);

        $messages = $this->getMessagesToRebroadcast($user, $lastTweetID);

        foreach ($messages as $message) {
            if ($message->getID() > $lastTweetID) {
                $lastTweetID = $message->getID();
            }
        }
        $this->app->db->updateValue($this->displayName . " " . $this::LAST_TWEET_ID, $lastTweetID);
        $this->app->logger->debug($this->displayName . " Latest tweet id stored", [$lastTweetID]);

        return $messages;
    }

    private function getUserInfo()
    {
        $parameters = [
            "screen_name"      => $this->config["screenName"],
            "include_entities" => false,
        ];

        return $this->connection->get("users/show", $parameters);
    }

    private function fetchRawMessagesFmAPI(int $id, string $lastTweetID): array
    {
        $parameters = [
            "user_id"         => $id,
            "include_rts"     => false,
            "trim_user"       => true,
            "exclude_replies" => false,
            "tweet_mode"      => "extended",
        ];
        if (!$lastTweetID) {
            $parameters["count"] = 1;
        } else {
            $parameters["since_id"] = $lastTweetID;
        }
        $this->app->logger->debug($this->displayName . " Making Call with Params:", [$parameters]);

        return $this->connection->get("statuses/user_timeline", $parameters);
    }

    private function filterRawMessages(int $userId, array $results): array
    {
        $idList    = [];
        $blackList = [];
        foreach ($results as $result) {
            $idList[] = $result->id;
        }
        foreach ($results as $index => $result) {
            if ((null != $result->in_reply_to_status_id && !in_array($result->in_reply_to_status_id, $idList) &&
                 !$this->IAmTheAuthorOf($result->in_reply_to_status_id, $userId)) ||
                $result->user->id != $userId) {
                $blackList[$index] = $index;
                $this->app->logger->debug($this->displayName . " Blacklisting Message id ", [$result->id]);
            }
        }
        foreach ($blackList as $index) {
            unset($results[$index]);
        }

        return $results;
    }

    private function createMessage(stdClass $message_raw, stdClass $user): Message
    {
        $message = new Message($this->app, json_encode($message_raw));
        $message->setId($message_raw->id);
        $message->setUrl("https://twitter.com/" . $user->id . "/status/" . $message->getId());
        $message->setSource($this->displayName, json_encode($user));
        $message->setDate(new DateTimeImmutable($message_raw->created_at));
        $message->setBody($message_raw->full_text);
        if (isset($message_raw->extended_entities->media)) {
            $image_urls = [];
            foreach ($message_raw->extended_entities->media as $image) {
                $image_urls[] = $image->media_url_https;
            }
            $message->setImages($image_urls);
        }
        if (isset($message_raw->entities->urls)) {
            $attachment_urls = [];
            foreach ($message_raw->entities->urls as $url) {
                $attachment_urls[] = $url->expanded_url;
            }
            $message->setAttachments($attachment_urls);
        }

        return $message;
    }

    private function iAmTheAuthorOf($statusId, int $userId): bool
    {
        $parameters = [
            "id" => $statusId,
        ];
        $this->app->logger->debug(__CLASS__ . " Checking if I wrote:", [$statusId]);
        $response = $this->connection->get("statuses/show", $parameters);

        $this->app->logger->debug(__CLASS__ . " I did write it:", [$response->user->id == $userId]);

        return $response->user->id == $userId;
    }

    public static function factory(Application $app, string $displayName, $config, TwitterOAuth $twitterOAuth = null):
    FetchInterface {
        if ($twitterOAuth === null) {
            $twitterOAuth = new TwitterOAuth(
                $config['consumerKey'],
                $config['consumerSecret'],
                $config['accessToken'],
                $config['accessTokenSecret']
            );
        }

        return new self($app, $displayName, $twitterOAuth, $config);

    }
}
