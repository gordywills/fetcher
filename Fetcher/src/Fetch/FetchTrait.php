<?php

namespace Fetcher\Fetch;

use Fetcher\Message\Messages;
use stdClass;

trait FetchTrait
{
    private function getMessagesToRebroadcast(stdClass $user, string $lastID): Messages
    {
        $result = $this->fetchRawMessagesFmAPI($user->id, $lastID);
        $result = $this->filterRawMessages($user->id, $result);

        $messages = new Messages();
        foreach ($result as $message_raw) {
            $message    = $this->createMessage($message_raw, $user);
            $messages[] = $message;
        }

        $messages->sortMessages();

        return $messages;
    }
}
