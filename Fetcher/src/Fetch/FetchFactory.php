<?php

namespace Fetcher\Fetch;

use Fetcher\Application;

class FetchFactory
{
    private static $map = [
        "twitter"  => TwitterFetch::class,
        "facebook" => FacebookFetch::class,
        "rss"      => RSSFetch::class,
    ];

    public static function factory(Application $app, string $name, string $classType, array $config): ?FetchInterface
    {
        if (!isset(self::$map[$classType])) {
            return null;
        }
        $app->logger->debug("Creating Fetcher $name");

        return self::$map[$classType]::factory($app, $name, $config);
    }

    public static function registerFetcher(string $classType, string $FQCN)
    {
        if (isset(self::$map[$classType])) {
            throw new FetchError("Trying to register existing fetcher - please rename your fetcher");
        }
        self::$map[$classType] = $FQCN;
    }
}
