<?php

namespace Fetcher\Fetch;

use DateTimeImmutable;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;
use Facebook\GraphNodes\GraphEdge;
use Facebook\GraphNodes\GraphNode;
use Fetcher\Application;
use Fetcher\Message\Message;
use Fetcher\Message\Messages;
use stdClass;

class FacebookFetch implements FetchInterface
{
    use FetchTrait;

    const LAST_POST_TS = "lastPostTS";
    private $app;
    private $connection;
    private $config;
    private $displayName = "Facebook";

    public function __construct(Application $app, string $displayName, Facebook $facebook, array $config)
    {
        $this->app         = $app;
        $this->connection  = $facebook;
        $this->config      = $config;
        $this->displayName = $displayName;
    }

    public function fetchMessages(): Messages
    {
        $this->app->logger->debug($this->displayName . " Getting Messages");
        $user       = $this->getUserInfo();
        $lastPostTS = $this->app->db->getValue($this->displayName . " " . $this::LAST_POST_TS);
        $this->app->logger->debug($this->displayName . " last known post TS", [$lastPostTS]);

        $messages = $this->getMessagesToRebroadcast($user, $lastPostTS);

        foreach ($messages as $message) {
            if ($message->getDate()->getTimestamp() > $lastPostTS) {
                $lastPostTS = $message->getDate()->getTimestamp();
            }
        }
        $this->app->db->updateValue($this->displayName . " " . $this::LAST_POST_TS, $lastPostTS);
        $this->app->logger->debug($this->displayName . " Latest post ts stored", [$lastPostTS]);

        return $messages;
    }

    private function getUserInfo()
    {
        try {
            $response = $this->connection->get(
                '/me'
            );
        } catch (FacebookSDKException $e) {
            $this->app->logger->debug('Facebook SDK returned an error: ' . $e->getMessage());
            throw new FetchError('Facebook SDK returned an error: ' . $e->getMessage());
        }

        try {
            $idNode = $response->getGraphNode();
        } catch (FacebookSDKException $e) {
            $this->app->logger->debug('Facebook SDK returned an error: ' . $e->getMessage());
            throw new FetchError('Facebook SDK returned an error: ' . $e->getMessage());
        }

        return new class($idNode->getField('name'), $idNode->getField('id')) extends stdClass
        {
            public $name;
            public $id;

            public function __construct($name, $id)
            {
                $this->name = $name;
                $this->id   = $id;
            }
        };
    }

    private function fetchRawMessagesFmAPI(string $page, string $lastPostTS): GraphEdge
    {
        $parameters = [
            "fields" => 'created_time,full_picture,id,from,permalink_url,attachments{url},message',
        ];
        if (!$lastPostTS) {
            $parameters["limit"] = 1;
        } else {
            $parameters["since"] = $lastPostTS;
        }
        $this->app->logger->debug($this->displayName . " Making Call with Params:", [$parameters]);
        $getParams = http_build_query($parameters);
        try {
            $response = $this->connection->get(
                '/' . $page . '/posts?' . $getParams
            );
        } catch (FacebookSDKException $e) {
            $this->app->logger->debug('Facebook SDK returned an error: ' . $e->getMessage());
            throw new FetchError('Facebook SDK returned an error: ' . $e->getMessage());
        }

        try {
            return $response->getGraphEdge();
        } catch (FacebookSDKException $e) {
            $this->app->logger->debug('Facebook SDK returned an error: ' . $e->getMessage());
            throw new FetchError('Facebook SDK returned an error: ' . $e->getMessage());
        }
    }

    private function filterRawMessages(string $page, GraphEdge $results): GraphEdge
    {
        $blackList = [];
        foreach ($results as $index => $node) {
            if ($node->getField('from')->getField('id') != $page) {
                $blackList[$index] = $index;
                $this->app->logger->debug($this->displayName . " Blacklisting Message id ", [$node->getField('id')]);
            }
        }
        foreach ($blackList as $index) {
            unset($results[$index]);
        }

        return $results;
    }

    private function createMessage(GraphNode $message_raw, stdClass $user): Message
    {
        $message = new Message($this->app, $message_raw->asJson());
        $message->setId($message_raw->getField('id'));
        $message->setUrl($message_raw->getField('permalink_url'));
        $message->setSource($this->displayName, json_encode($user));
        $message->setDate(DateTimeImmutable::createFromMutable($message_raw->getField('created_time')));
        $message->setBody($message_raw->getField('message') ?: '');
        if (!is_null($message_raw->getField('full_picture', null))) {
            $message->setImages([$message_raw->getField('full_picture')]);
        }

        return $message;
    }

    public static function factory(
        Application $app,
        string $displayName,
        $config,
        Facebook $facebook = null
    ): FetchInterface {
        if ($facebook === null) {
            try {
                $facebook = new Facebook([
                    'app_id'                => $config['app_id'],
                    'app_secret'            => $config['app_secret'],
                    'default_graph_version' => $config['graph_version'],
                    'default_access_token'  => $config['access_token'],
                ]);
            } catch (FacebookSDKException $e) {
                $app->logger->debug('Facebook SDK returned an error: ' . $e->getMessage());
                throw new FetchError('Facebook SDK returned an error: ' . $e->getMessage());
            }
        }

        return new self($app, $displayName, $facebook, $config);
    }
}
