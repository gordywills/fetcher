<?php

namespace Fetcher\Fetch;

use Fetcher\Application;
use Fetcher\Message\Messages;

interface FetchInterface
{
    public static function factory(Application $app, string $displayName, array $config): FetchInterface;

    public function fetchMessages(): Messages;
}
