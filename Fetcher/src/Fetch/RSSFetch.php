<?php

namespace Fetcher\Fetch;

use DateTimeImmutable;
use Fetcher\Application;
use Fetcher\Message\Message;
use Fetcher\Message\Messages;
use stdClass;
use Zend\Feed\Reader\Entry\EntryInterface;
use Zend\Feed\Reader\Feed\FeedInterface;
use Zend\Feed\Reader\Reader;

class RSSFetch implements FetchInterface
{
    use FetchTrait;
    const LAST_BLOG_TS = "lastBlogTS";
    private $connection;
    private $config;
    private $app;
    private $displayName = "twitter";

    public function __construct(Application $app, string $displayName, FeedInterface $rss_conn, array $config)
    {
        $this->app         = $app;
        $this->connection  = $rss_conn;
        $this->config      = $config;
        $this->displayName = $displayName;
    }

    public function fetchMessages(): Messages
    {
        $this->app->logger->debug($this->displayName . " Getting Messages");
        $user       = $this->getUserInfo();
        $lastBlogTS = $this->app->db->getValue($this->displayName . " " . $this::LAST_BLOG_TS);
        $this->app->logger->debug($this->displayName . " last known Blog Time", [$lastBlogTS]);

        $messages = $this->getMessagesToRebroadcast($user, $lastBlogTS);

        foreach ($messages as $message) {
            if ($message->getDate()->getTimestamp() > $lastBlogTS) {
                $lastBlogTS = $message->getDate()->getTimestamp();
            }
        }
        $this->app->db->updateValue($this->displayName . " " . $this::LAST_BLOG_TS, $lastBlogTS);
        $this->app->logger->debug($this->displayName . " Latest post ts stored", [$lastBlogTS]);

        return $messages;
    }

    private function getUserInfo(): stdClass
    {
        return new class($this->config['rssUrl']) extends stdClass
        {
            public $id;

            public function __construct(string $id)
            {
                $this->id = $id;
            }
        };
    }

    public static function factory(Application $app, string $displayName, array $config, FeedInterface $RSSConn = null):
    FetchInterface {
        if ($RSSConn === null) {
            $RSSConn = Reader::import($config['rssUrl']);
        }

        return new self($app, $displayName, $RSSConn, $config);
    }

    private function fetchRawMessagesFmAPI($id, $lastBlogTS)
    {
        $return = [];
        foreach ($this->connection as $item) {
            if ($lastBlogTS == "") {
                $return[]   = $item;
                $lastBlogTS = $item->getDateCreated()->getTimestamp();
            }
            if ($item->getDateCreated()->getTimestamp() > $lastBlogTS) {
                $return[] = $item;
            }
        }

        return $return;
    }

    private function filterRawMessages($id, $result)
    {
        return $result;
    }

    private function createMessage(EntryInterface $message_raw, stdClass $user): Message
    {
        $message = new Message($this->app, json_encode($message_raw));
        $message->setId($message_raw->getId());
        $message->setUrl($message_raw->getPermalink());
        $message->setSource($this->displayName, json_encode($user));
        $message->setDate(DateTimeImmutable::createFromMutable($message_raw->getDateCreated()));
        $description = $message_raw->getDescription();
        $description = preg_replace('/.*?href="(.*?)">.*/', 'Read More at $1', $description);
        $description = strip_tags($description);
        $message->setBody($description);
        $message->setTitle($message_raw->getTitle());

        $content = $message_raw->getContent();
        preg_match_all('/<img.*src="(.*?)".*\/>/', $content, $output_array);
        if (isset($output_array[1])) {
            $message->setImages($output_array[1]);
        }

        return $message;
    }
}
