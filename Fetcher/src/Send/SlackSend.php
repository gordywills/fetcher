<?php

namespace Fetcher\Send;

use Fetcher\Application;
use Fetcher\Message\Messages;
use Maknz\Slack\Client;

class SlackSend implements SendInterface
{
    private $connection;
    private $config;
    private $app;
    private $displayName;

    public function __construct(Application $app, string $displayName, Client $slack, array $config)
    {
        $this->app         = $app;
        $this->connection  = $slack;
        $this->config      = $config;
        $this->displayName = $displayName;
    }

    public function sendMessages(Messages $messages)
    {
        foreach ($messages as $message) {
            $slack  = $this->connection->createMessage();
            $source = $message->getSource();
            $slack->from($source['service'] . " Bot");
            $message_body =
                'At: ' . $message->getDate()->format("H:i j-M-y") . "\n" .
                'From: ' . ucwords($source['service']) . ' ' .
                $message->getUrl() . "\n\n" .
                $message->getBody();
            $slack->setText($message_body);
            foreach ($message->getImages() as $image_url) {
                $slack->attach([
                    'image_url' => $image_url,
                ]);
            }
            foreach ($message->getAttachments() as $attachment_url) {
                $slack->attach([
                    'text' => $attachment_url,
                ]);
            }
            $slack->send();
        }
    }

    public static function factory(Application $app, string $displayName, array $config, Client $slack = null):
    SendInterface {
        if ($slack === null) {
            $settings = [
                "channel"  => $config["channel"],
                "username" => $config["user"],
                "icon"     => $config["icon"],
            ];
            $slack    = new Client(
                $config["hook"],
                $settings
            );
        }

        return new self($app, $displayName, $slack, $config);
    }

}
