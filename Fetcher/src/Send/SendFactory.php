<?php

namespace Fetcher\Send;

use Fetcher\Application;

class SendFactory
{
    private static $map = [
        "slack" => SlackSend::class,
    ];

    public static function factory(Application $app, string $name, string $classType, array $config): ?SendInterface
    {
        if (!isset(self::$map[$classType])) {
            return null;
        }
        $app->logger->debug("Creating Sender $name");

        return self::$map[$classType]::factory($app, $name, $config);
    }

    public static function registerSender(string $classType, string $FQCN)
    {
        if (isset(self::$map[$classType])) {
            throw new SendError("Trying to register existing fetcher - please rename your fetcher");
        }
        self::$map[$classType] = $FQCN;
    }
}
