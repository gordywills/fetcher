<?php

namespace Fetcher\Send;

use Fetcher\Application;
use Fetcher\Message\Messages;

interface SendInterface
{
    public static function factory(Application $app, string $displayName, array $config): SendInterface;

    public function sendMessages(Messages $messages);
}
