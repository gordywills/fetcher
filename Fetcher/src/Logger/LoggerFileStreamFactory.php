<?php

namespace Fetcher\Logger;

use Exception;
use InvalidArgumentException;
use Monolog\Handler\StreamHandler;

class LoggerFileStreamFactory
{
    /**
     * @var StreamHandler[]
     */
    private static $logFileHandlers = [];

    public static function factory(string $logFilePath, string $logLevel): StreamHandler
    {
        $handlerKey = sha1($logFilePath . $logLevel);
        if (!isset(self::$logFileHandlers[$handlerKey])) {
            try {
                self::$logFileHandlers[$handlerKey] = new StreamHandler(
                    getcwd() . DIRECTORY_SEPARATOR . $logFilePath,
                    $logLevel
                );
            } catch (InvalidArgumentException $e) {
                throw new LoggerError("Error setting up logs please check logFilePath and logLevel are valid", 0, $e);
            } catch (Exception $e) {
                throw new LoggerError("Unable to create the log file - check logFilePath in config is valid", 0, $e);
            }
        }

        return self::$logFileHandlers[$handlerKey];
    }
}
