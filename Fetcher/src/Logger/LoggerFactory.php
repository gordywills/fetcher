<?php

namespace Fetcher\Logger;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class LoggerFactory
{

    public static function factory(StreamHandler $fileStreamHandler, ?string $channelName = null): Logger
    {
        if (null === $channelName) {
            $channelName = "mainLog";
        }
        $logger = new Logger($channelName);
        $logger->pushHandler($fileStreamHandler);

        return $logger;
    }

}
