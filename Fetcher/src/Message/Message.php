<?php

namespace Fetcher\Message;

use DateTimeImmutable;
use Fetcher\Application;

class Message
{
    private $source      = null;
    private $date        = null;
    private $title       = null;
    private $body        = null;
    private $images      = [];
    private $attachments = [];
    private $id          = null;
    private $rawMessage  = null;
    private $url         = null;

    private $app;

    public function __construct(Application $app, string $message)
    {
        $this->app = $app;
        $this->setRawMessage($message);
    }

    public function getSource(): ?array
    {
        return $this->source;
    }

    public function setSource(string $service, string $user): void
    {
        $this->source["service"] = $service;
        $this->source["user"]    = stripcslashes($user);
    }

    public function getDate(): ?DateTimeImmutable
    {
        return $this->date;
    }

    public function setDate(DateTimeImmutable $date): void
    {
        $this->date = $date;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = stripcslashes($title);
    }

    public function getBody(): string
    {
        return $this->body;
    }

    public function setBody(?string $body = null): void
    {
        if (is_null($body)) {
            $body = '';
        }
        $this->body = stripcslashes($body);
    }

    public function getImages(): array
    {
        return $this->images;
    }

    public function setImages(array $images): void
    {
        foreach ($images as $image) {
            $this->images[] = stripcslashes($image);
        }
    }

    public function getAttachments(): array
    {
        return $this->attachments;
    }

    public function setAttachments(array $attachments): void
    {
        foreach ($attachments as $attachment) {
            $this->attachments[] = stripcslashes($attachment);
        }
    }

    public function getRawMessage(): string
    {
        return $this->rawMessage;
    }

    public function setRawMessage(string $rawMessage): void
    {
        $this->rawMessage = stripcslashes($rawMessage);
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function setUrl(string $url): void
    {
        $this->url = stripcslashes($url);
    }

}
