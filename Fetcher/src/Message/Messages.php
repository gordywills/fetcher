<?php

namespace Fetcher\Message;

use ArrayAccess;
use Countable;
use Iterator;
use TypeError;

class Messages implements ArrayAccess, Countable, Iterator
{
    private $position  = 0;
    private $container = [];

    public static function cmp(Message $a, Message $b)
    {
        if ($a->getDate() === $b->getDate()) {
            return 0;
        }

        return $a->getDate() < $b->getDate() ? -1 : 1;
    }

    public function offsetSet($offset, $value)
    {
        if (!$value instanceof Message) {
            throw new TypeError(gettype($value) . " must be of type Message");
        }

        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    public function count()
    {
        return count($this->container);
    }

    public function current()
    {
        return $this->container[$this->position];
    }

    public function next()
    {
        ++$this->position;
    }

    public function key()
    {
        return $this->position;
    }

    public function valid()
    {
        return isset($this->container[$this->position]);
    }

    public function rewind()
    {
        $this->position = 0;
    }

    public function sortMessages(): Messages
    {
        usort($this->container, array($this, "cmp"));

        return $this;
    }
}
