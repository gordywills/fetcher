<?php

namespace Fetcher;

use Error;
use Fetcher\DB\DBFactory;
use Fetcher\Fetch\FetchFactory;
use Fetcher\Logger\LoggerFactory;
use Fetcher\Logger\LoggerFileStreamFactory;
use Fetcher\Message\Messages;
use Fetcher\Send\SendFactory;

class Application
{
    const APP_LOCKFILE_PHP = 'app.lockfile.php';
    public $config = null;
    public $db     = null;
    public $logger = null;

    public function __construct(string $configFile = 'app.config.ini')
    {
        $this->config  = parse_ini_file($configFile, true, INI_SCANNER_RAW);
        $configGeneral = $this->config['general'];
        $this->logger  = LoggerFactory::factory(
            LoggerFileStreamFactory::factory(
                $configGeneral['logFilePath'],
                $configGeneral['logLevel']
            ),
            'mainLog'
        );
        $this->logger->info("###################");
        $this->logger->info("Application started", [date(DATE_RFC1036)]);
        $this->logger->debug("Config loaded from file", [$configFile]);
        $this->runtimeCheck();
        $this->db = DBFactory::factory($this, $configGeneral['databaseFile']);
        register_shutdown_function([$this, "logEnd"]);
    }

    private function runtimeCheck(): void
    {
        $this->cliCheck();
        $this->lockFileCheck();
        $this->writeLockFile(true);
        $this->lockFileCheck();
        register_shutdown_function([$this, "writeLockFile"], false);
        $this->logger->debug("Runtime Checks passed");
    }

    private function cliCheck(): void
    {
        if ('cli' != PHP_SAPI) {
            echo $msg = "This script must be run using php-cli";
            throw new Error($msg);
        }
    }

    private function lockFileCheck(): void
    {
        $processId = getmypid();
        if (!file_exists(self::APP_LOCKFILE_PHP)) {
            $return = false;
        } else {
            $return = require self::APP_LOCKFILE_PHP;
        }
        if ($return && $return != $processId) {
            $this->logger->info("My Process Id does not match the lock file exiting:", [$processId, $return]);
            exit(1);
        }
    }

    public function writeLockFile(bool $locked): void
    {
        if ($locked) {
            $locked = getmypid();
        } else {
            $locked = "false";
        }
        $contents = <<<DOC
<?php
return $locked;

DOC;
        file_put_contents(self::APP_LOCKFILE_PHP, $contents);
    }

    public function logEnd(): void
    {
        $this->logger->info("Application completed.", [date(DATE_RFC1036)]);
        $this->logger->info("######################");
    }

    public function fetchAllMessages(): Messages
    {
        $fetchers = [];
        foreach ($this->config as $key => $config) {
            if ($key == "general" || $config['direction'] !== "fetch") {
                continue;
            }
            $fetchers[] = FetchFactory::factory($this, $config['displayName'], $config['classType'], $config);
        }
        $allMessages = new Messages();
        foreach ($fetchers as $fetcher) {
            if ($fetcher === null) {
                continue;
            }
            $messages = $fetcher->fetchMessages();
            $this->logger->info(get_class($fetcher) . " Fetched " . count($messages) . " messages");
            foreach ($messages as $message) {
                $allMessages[] = $message;
            }
        }

        return $allMessages;
    }

    public function sendAllMessages(Messages $messages): void
    {
        $senders = [];
        foreach ($this->config as $key => $config) {
            if ($key == "general" || $config['direction'] !== "send") {
                continue;
            }
            $senders[] = SendFactory::factory($this, $config['displayName'], $config['classType'], $config);
        }
        foreach ($senders as $sender) {
            if ($sender === null) {
                continue;
            }
            $this->logger->info(get_class($sender) . " Sending " . count($messages) . " messages");
            $sender->sendMessages($messages);
        }
    }
}
