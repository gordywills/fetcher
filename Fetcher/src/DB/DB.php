<?php

namespace Fetcher\DB;

use Fetcher\Application;
use SQLite3;

class DB
{
    private $db;
    private $app;

    public function __construct(Application $app, SQLite3 $dbConn)
    {
        $this->app = $app;
        $this->db  = $dbConn;
        $this->prepSchema();
    }

    private function prepSchema()
    {
        $exists = $this->db->query("SELECT name FROM sqlite_master WHERE type='table' AND name='kvStore';");
        if (!$exists->fetchArray()) {
            $this->app->logger->info("Schema does not exist - creating it now");
            $this->db->exec(
                "CREATE TABLE IF not exists kvStore (k CHAR(255) NOT NULL PRIMARY KEY, v CHAR(255) NOT NULL)"
            );
        }
    }

    public function getValue(string $key): string
    {
        $key    = str_replace(" ", "-", $key);
        $result = $this->db->query("SELECT k, v FROM kvstore WHERE k = '$key'");
        if (!$result) {
            $this->app->logger->info(__METHOD__ . " failed for $key");

            return "";
        }
        $array = $result->fetchArray();
        if (!$array) {
            $this->app->logger->debug("Getting $key from db: no results");

            return "";
        }
        $this->app->logger->debug("Getting $key from db: ", [$array['v']]);

        return $array['v'];
    }

    public function updateValue(string $key, string $value): bool
    {
        $this->app->logger->debug("Updating $key in db: $value");

        return $this->storeValue($key, $value);
    }

    public function storeValue(string $key, string $value): bool
    {
        $key = str_replace(" ", "-", $key);
        if (strlen($key) > 255) {
            throw new DBKeyError("Key length too long for: $key");
        }
        if (strlen($value) > 255) {
            throw new DBValueError("Value length too long for: $value");
        }
        $result = $this->db->exec("REPLACE INTO kvstore(k, v) VALUES('$key', '$value')");
        if (!$result) {
            return false;
        }
        $this->app->logger->debug("Storing $key in db: $value");

        return $this->db->changes() > 0;
    }
}
