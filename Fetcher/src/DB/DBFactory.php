<?php

namespace Fetcher\DB;

use Fetcher\Application;
use SQLite3;

class DBFactory
{
    public static function factory(Application $app, string $dbFile = "fetcher.db"): DB
    {
        $app->logger->debug("Creating DB Connection");

        return new DB($app, new SQLite3($dbFile));
    }
}
