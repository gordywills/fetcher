<?php

namespace TestHelpers;

use ArrayAccess;
use Countable;
use Iterator;
use stdClass;

class myIterator extends stdClass implements Iterator, Countable, ArrayAccess
{
    public function current()
    {
    }

    public function next()
    {
    }

    public function key()
    {
    }

    public function valid()
    {
    }

    public function rewind()
    {
    }

    public function count()
    {
    }

    public function offsetExists($offset)
    {
    }

    public function offsetGet($offset)
    {
    }

    public function offsetSet($offset, $value)
    {
    }

    public function offsetUnset($offset)
    {
    }
}