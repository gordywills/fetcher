<?php

namespace TestHelpers;

use PHPUnit\Framework\MockObject\MockObject;
use stdClass;

trait IetratorTestHelperTrait
{
    public function mockIterator(MockObject $iteratorMock, array $items)
    {
        $iteratorData           = new stdClass();
        $iteratorData->array    = $items;
        $iteratorData->position = 0;

        $iteratorMock->expects($this->any())
                     ->method('rewind')
                     ->will(
                         $this->returnCallback(
                             function () use ($iteratorData)
                             {
                                 $iteratorData->position = 0;
                             }
                         )
                     );

        $iteratorMock->expects($this->any())
                     ->method('current')
                     ->will(
                         $this->returnCallback(
                             function () use ($iteratorData)
                             {
                                 return $iteratorData->array[$iteratorData->position];
                             }
                         )
                     );

        $iteratorMock->expects($this->any())
                     ->method('key')
                     ->will(
                         $this->returnCallback(
                             function () use ($iteratorData)
                             {
                                 return $iteratorData->position;
                             }
                         )
                     );

        $iteratorMock->expects($this->any())
                     ->method('next')
                     ->will(
                         $this->returnCallback(
                             function () use ($iteratorData)
                             {
                                 $iteratorData->position++;
                             }
                         )
                     );

        $iteratorMock->expects($this->any())
                     ->method('valid')
                     ->will(
                         $this->returnCallback(
                             function () use ($iteratorData)
                             {
                                 return isset($iteratorData->array[$iteratorData->position]);
                             }
                         )
                     );

        $iteratorMock->expects($this->any())
                     ->method('count')
                     ->will(
                         $this->returnCallback(
                             function () use ($iteratorData)
                             {
                                 return sizeof($iteratorData->array);
                             }
                         )
                     );

        return $iteratorMock;
    }
}