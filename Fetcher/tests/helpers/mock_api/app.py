import os
from flask import request, Flask

circle_token = os.environ.get("CIRCLE_CI_TOKEN")
secret = os.environ.get("WEBHOOK_SECRET")


def create_app():
    app = Flask(__name__)

    @app.route('/', defaults={'path': ''}, methods=['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD'])
    @app.route('/<path:path>')
    def responder(path):
        input_json = request.get_data()
        input_args = request.args
        input_method = request.method
        input_headers = request.headers
        return_string = request.full_path + "\n" + input_method + "\n" + stringify_args(input_headers) + "\n" + stringify_args(
            input_args) + "\n" + (input_json if input_json is not None else 'no json') + "\n"
        print(return_string)
        return return_string, 200

    def stringify_args(args):
        string = ''
        for k, v in args.items():
            string += k + ' => ' + v + "\n"
        return string

    return app
