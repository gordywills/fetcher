from waitress import serve
from .app import create_app

SERVER_PORT = 5000
SERVER = "127.0.0.1"

if __name__ == '__main__':
    app = create_app()
    serve(app, host=SERVER, port=SERVER_PORT)