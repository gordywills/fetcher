<?php

use Fetcher\Application;
use Fetcher\DB\DB;
use Fetcher\DB\DBKeyError;
use Fetcher\DB\DBValueError;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;

class DBTest extends TestCase
{
    private $application;

    public function setUp(): void
    {
        $this->application         = $this->getMockBuilder(Application::class)->disableOriginalConstructor()->getMock();
        $logger                    =
            $this->getMockBuilder(Logger::class)->disableOriginalConstructor()->getMock();
        $this->application->logger = $logger;
    }

    public function testConstructorBrandNew()
    {
        $response = $this->getMockBuilder(SQLite3Result::class)->disableOriginalConstructor()->getMock();
        $response->expects($this->once())->method('fetchArray')->will($this->returnValue(false));
        $db = $this->getMockBuilder(SQLite3::class)->disableOriginalConstructor()->getMock();
        $db->expects($this->once())->method('query')->will($this->returnValue($response));
        $db->expects($this->once())->method('exec')->will($this->returnValue(true));
        $dbConn = new DB($this->application, $db);
        unset($dbConn);
    }

    public function testConstructorExisting()
    {
        $response = $this->getMockBuilder(SQLite3Result::class)->disableOriginalConstructor()->getMock();
        $response->expects($this->once())->method('fetchArray')->will($this->returnValue(true));
        $db = $this->getMockBuilder(SQLite3::class)->disableOriginalConstructor()->getMock();
        $db->expects($this->once())->method('query')->will($this->returnValue($response));
        $db->expects($this->never())->method('exec')->will($this->returnValue(true));
        $dbConn = new DB($this->application, $db);
        unset($dbConn);
    }

    public function testStoreValueKeyTooBig()
    {
        $response = $this->getMockBuilder(SQLite3Result::class)->disableOriginalConstructor()->getMock();
        $response->expects($this->atMost(2))->method('fetchArray')->will($this->returnValue(true));
        $db = $this->getMockBuilder(SQLite3::class)->disableOriginalConstructor()->getMock();
        $db->expects($this->once())->method('query')->will($this->returnValue($response));
        $dbConn = new DB($this->application, $db);
        $key    =
            "11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111";
        $val    = "1";
        $this->expectException(DBKeyError::class);
        $dbConn->storeValue($key, $val);
    }

    public function testStoreValueValueTooBig()
    {
        $response = $this->getMockBuilder(SQLite3Result::class)->disableOriginalConstructor()->getMock();
        $response->expects($this->atMost(2))->method('fetchArray')->will($this->returnValue(true));
        $db = $this->getMockBuilder(SQLite3::class)->disableOriginalConstructor()->getMock();
        $db->expects($this->once())->method('query')->will($this->returnValue($response));
        $dbConn = new DB($this->application, $db);
        $val    =
            "11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111";
        $key    = "1";
        $this->expectException(DBValueError::class);
        $dbConn->storeValue($key, $val);
    }

    public function testStoreValueQueryFailure()
    {
        $response = $this->getMockBuilder(SQLite3Result::class)->disableOriginalConstructor()->getMock();
        $response->expects($this->atMost(2))->method('fetchArray')->will($this->returnValue(true));
        $db = $this->getMockBuilder(SQLite3::class)->disableOriginalConstructor()->getMock();
        $db->expects($this->once())->method('query')->will($this->returnValue($response));
        $db->expects($this->once())->method('exec')->will($this->returnValue(false));
        $dbConn = new DB($this->application, $db);
        $val    = "1";
        $key    = "1";
        $result = $dbConn->storeValue($key, $val);
        $this->assertFalse($result);
    }

    public function testStoreValueZeroRows()
    {
        $response = $this->getMockBuilder(SQLite3Result::class)->disableOriginalConstructor()->getMock();
        $response->expects($this->atMost(2))->method('fetchArray')->will($this->returnValue(true));
        $db = $this->getMockBuilder(SQLite3::class)->disableOriginalConstructor()->getMock();
        $db->expects($this->once())->method('query')->will($this->returnValue($response));
        $db->expects($this->once())->method('exec')->will($this->returnValue(true));
        $db->expects($this->once())->method('changes')->will($this->returnValue(0));
        $dbConn = new DB($this->application, $db);
        $val    = "1";
        $key    = "1";
        $result = $dbConn->storeValue($key, $val);
        $this->assertFalse($result);
    }

    public function testStoreValueOneRow()
    {
        $response = $this->getMockBuilder(SQLite3Result::class)->disableOriginalConstructor()->getMock();
        $response->expects($this->atMost(2))->method('fetchArray')->will($this->returnValue(true));
        $db = $this->getMockBuilder(SQLite3::class)->disableOriginalConstructor()->getMock();
        $db->expects($this->once())->method('query')->will($this->returnValue($response));
        $db->expects($this->once())->method('exec')->will($this->returnValue(true));
        $db->expects($this->once())->method('changes')->will($this->returnValue(1));
        $dbConn = new DB($this->application, $db);
        $val    = "1";
        $key    = "1";
        $result = $dbConn->storeValue($key, $val);
        $this->assertTrue($result);
    }

    public function testStoreValueMultiRows()
    {
        $response = $this->getMockBuilder(SQLite3Result::class)->disableOriginalConstructor()->getMock();
        $response->expects($this->atMost(2))->method('fetchArray')->will($this->returnValue(true));
        $db = $this->getMockBuilder(SQLite3::class)->disableOriginalConstructor()->getMock();
        $db->expects($this->once())->method('query')->will($this->returnValue($response));
        $db->expects($this->once())->method('exec')->will($this->returnValue(true));
        $db->expects($this->once())->method('changes')->will($this->returnValue(2));
        $dbConn = new DB($this->application, $db);
        $val    = "1";
        $key    = "1";
        $result = $dbConn->storeValue($key, $val);
        $this->assertTrue($result);
    }

    public function testGetValueQueryFailure()
    {
        $response = $this->getMockBuilder(SQLite3Result::class)->disableOriginalConstructor()->getMock();
        $response->expects($this->once())->method('fetchArray')->will($this->returnValue(true));
        $db = $this->getMockBuilder(SQLite3::class)->disableOriginalConstructor()->getMock();
        $db->expects($this->atMost(2))->method('query')->will($this->onConsecutiveCalls($response, false));
        $dbConn = new DB($this->application, $db);
        $key    = "1";
        $result = $dbConn->getValue($key);
        $this->assertEquals("", $result);
    }

    public function testGetValueEmptyArray()
    {
        $response = $this->getMockBuilder(SQLite3Result::class)->disableOriginalConstructor()->getMock();
        $response->expects($this->atMost(2))->method('fetchArray')->will($this->onConsecutiveCalls(true, []));
        $db = $this->getMockBuilder(SQLite3::class)->disableOriginalConstructor()->getMock();
        $db->expects($this->atMost(2))->method('query')->will($this->returnValue($response));
        $dbConn = new DB($this->application, $db);
        $key    = "1";
        $result = $dbConn->getValue($key);
        $this->assertEquals("", $result);
    }

    public function testGetValueResultArray()
    {
        $response = $this->getMockBuilder(SQLite3Result::class)->disableOriginalConstructor()->getMock();
        $response->expects($this->atMost(2))->method('fetchArray')->will($this->onConsecutiveCalls(true,
            ["v" => "winner"]));
        $db = $this->getMockBuilder(SQLite3::class)->disableOriginalConstructor()->getMock();
        $db->expects($this->atMost(2))->method('query')->will($this->returnValue($response));
        $dbConn = new DB($this->application, $db);
        $key    = "1";
        $result = $dbConn->getValue($key);
        $this->assertEquals("winner", $result);
    }

    public function testUpdateValueOneRow()
    {
        $response = $this->getMockBuilder(SQLite3Result::class)->disableOriginalConstructor()->getMock();
        $response->expects($this->atMost(2))->method('fetchArray')->will($this->returnValue(true));
        $db = $this->getMockBuilder(SQLite3::class)->disableOriginalConstructor()->getMock();
        $db->expects($this->once())->method('query')->will($this->returnValue($response));
        $db->expects($this->once())->method('exec')->will($this->returnValue(true));
        $db->expects($this->once())->method('changes')->will($this->returnValue(1));
        $dbConn = new DB($this->application, $db);
        $val    = "1";
        $key    = "1";
        $result = $dbConn->updateValue($key, $val);
        $this->assertTrue($result);
    }
}