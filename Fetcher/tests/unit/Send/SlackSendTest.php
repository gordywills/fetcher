<?php

use Fetcher\Application;
use Fetcher\Message\Messages;
use Fetcher\Send\SlackSend;
use Maknz\Slack\Client;
use Maknz\Slack\Message;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;

class SlackSendTest extends TestCase
{
    private $application;
    private $connection;

    public function setUp(): void
    {
        $this->application         = $this->getMockBuilder(Application::class)->disableOriginalConstructor()->getMock();
        $logger                    =
            $this->getMockBuilder(Logger::class)->disableOriginalConstructor()->getMock();
        $this->application->logger = $logger;
        $this->connection          =
            $this->getMockBuilder(Client::class)->disableOriginalConstructor()->getMock();
    }

    public function testConstructor()
    {
        $obj = new SlackSend($this->application, "mock", $this->connection, []);
        $this->assertInstanceOf(SlackSend::class, $obj);
    }

    public function testFactoryAllParams()
    {
        $obj = SlackSend::factory($this->application, "mock", [], $this->connection);
        $this->assertInstanceOf(SlackSend::class, $obj);
    }

    public function testFactoryReqParams()
    {
        $obj = SlackSend::factory($this->application, "mock",
            [
                "channel" => "test_chan",
                "user"    => "test_user",
                "icon"    => "test_icon",
                "hook"    => "http://not.a.thing",
            ]);
        $this->assertInstanceOf(SlackSend::class, $obj);
    }

    public function testSendMessages()
    {
        $slackMsg = $this->getMockBuilder(Message::class)->disableOriginalConstructor()->getMock();
        $slackMsg->expects($this->once())->method("from");
        $slackMsg->expects($this->once())->method("setText");
        $slackMsg->expects($this->once())->method("send");
        $slackMsg->expects($this->exactly(2))->method("attach");

        $this->connection->expects($this->once())->method("createMessage")->willReturn($slackMsg);

        $inboundMsg = $this->getMockBuilder(\Fetcher\Message\Message::class)->disableOriginalConstructor()->getMock();
        $inboundMsg->expects($this->once())->method("getSource")->willReturn(["service" => "twitter"]);
        $inboundMsg->expects($this->once())->method("getBody")->willReturn("SomeText");
        $inboundMsg->expects($this->once())->method("getImages")->willReturn(["image"]);
        $inboundMsg->expects($this->once())->method("getAttachments")->willReturn(["attachment"]);
        $inboundMsg->expects($this->once())->method("getDate")->willReturn(new DateTimeImmutable());

        $inboundMessages   = new Messages();
        $inboundMessages[] = $inboundMsg;
        $obj               = new SlackSend($this->application, "mock", $this->connection, []);
        $obj->sendMessages($inboundMessages);
    }
}