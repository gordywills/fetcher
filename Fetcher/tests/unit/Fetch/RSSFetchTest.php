<?php

use Fetcher\Application;
use Fetcher\DB\DB;
use Fetcher\Fetch\RSSFetch;
use Fetcher\Message\Messages;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;
use Zend\Feed\Reader\Feed\Rss;

class RSSFetchTest extends TestCase
{
    private $application;
    private $connection;

    public function setUp(): void
    {
        $this->application         = $this->getMockBuilder(Application::class)->disableOriginalConstructor()->getMock();
        $logger                    =
            $this->getMockBuilder(Logger::class)->disableOriginalConstructor()->getMock();
        $this->application->logger = $logger;
        $this->connection          = new Rss($this->DataObjectRSS());
    }

    private function DataObjectRSS()
    {
        $doc = new DOMDocument();
        $str = <<<DOC
<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0"
	xmlns:content="http://purl.org/rss/1.0/modules/content/"
	xmlns:wfw="http://wellformedweb.org/CommentAPI/"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:atom="http://www.w3.org/2005/Atom"
	xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
	xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
	>
    <channel>
        <title>A Blog</title>
        <atom:link href="https://www.notreal.org/blog/feed/" rel="self" type="application/rss+xml" />
        <link>https://www.notreal.org</link>
        <description>A Blog Described</description>
        <lastBuildDate>Wed, 11 Sep 2019 18:02:14 +0000</lastBuildDate>
        <language>en-GB</language>
        <sy:updatePeriod>hourly</sy:updatePeriod>
        <sy:updateFrequency>1</sy:updateFrequency>
        <generator>https://wordpress.org/?v=5.0.3</generator>
        <image>
            <url>https://www.notreal.org/wp-content/uploads/2018/10/logo.png</url>
            <title>Blog Icon</title>
            <link>https://www.notreal.org</link>
            <width>32</width>
            <height>32</height>
        </image> 
        <item>
            <title>Bananas</title>
            <link>https://www.notreal.org/bananas/</link>
            <comments>https://www.notreal.org/bananas/#respond</comments>
            <pubDate>Thu, 11 Jul 2019 14:51:46 +0000</pubDate>
            <creator><![CDATA[A Person]]></creator>
            <dc:category><![CDATA[Fruit]]></dc:category>
            <guid isPermaLink="false">https://www.notreal.org/?p=123</guid>
            <description><![CDATA[This is the description]]></description>
            <content:encoded><![CDATA[This is the content <img src="https://www.notreal.org/one.jpg"/> this is some 
            more content <img src="https://www.notreal.org/two.jpg"/>
            ]]></content:encoded>
            <wfw:commentRss>https://www.notreal.org/bananas/feed/</wfw:commentRss>
            <slash:comments>0</slash:comments>
		</item>
		<item>
            <title>Pears</title>
            <link>https://www.notreal.org/pears/</link>
            <comments>https://www.notreal.org/pears/#respond</comments>
            <pubDate>Wed, 10 Jul 2019 14:51:46 +0000</pubDate>
            <creator><![CDATA[A Person]]></creator>
            <dc:category><![CDATA[Fruit]]></dc:category>
            <guid isPermaLink="false">https://www.notreal.org/?p=122</guid>
            <description><![CDATA[This is the description]]></description>
            <content:encoded><![CDATA[This is the content <img src="https://www.notreal.org/four.jpg"/> this is some 
            more content <img src="https://www.notreal.org/three.jpg"/>]]></content:encoded>
            <wfw:commentRss>https://www.notreal.org/pears/feed/</wfw:commentRss>
            <slash:comments>0</slash:comments>
		</item>
		<item>
            <title>Apples</title>
            <link>https://www.notreal.org/apples/</link>
            <comments>https://www.notreal.org/apples/#respond</comments>
            <pubDate>Tue, 09 Jul 2019 14:51:46 +0000</pubDate>
            <creator><![CDATA[A Person]]></creator>
            <dc:category><![CDATA[Fruit]]></dc:category>
            <guid isPermaLink="false">https://www.notreal.org/?p=121</guid>
            <description><![CDATA[This is the description]]></description>
            <content:encoded><![CDATA[This is the content]]></content:encoded>
            <wfw:commentRss>https://www.notreal.org/pears/feed/</wfw:commentRss>
            <slash:comments>0</slash:comments>
		</item>
	</channel>
</rss>
DOC;
        $doc->loadXML($str);

        return $doc;
    }

    public function testConstructor()
    {
        $obj = new RSSFetch($this->application, "mock", $this->connection, []);
        $this->assertInstanceOf(RSSFetch::class, $obj);
    }

    public function testFactoryAllParams()
    {
        $obj = RSSFetch::factory($this->application, "mock", [], $this->connection);
        $this->assertInstanceOf(RSSFetch::class, $obj);
    }

    public function testFactoryReqParams()
    {
        $obj = RSSFetch::factory($this->application, "mock",
            ['rssUrl' => "https://feeds.bbci.co.uk/news/rss.xml"]);
        $this->assertInstanceOf(RSSFetch::class, $obj);
    }

    public function testGetMessagesNormalRoute()
    {
        $db = $this->getMockBuilder(DB::class)->disableOriginalConstructor()->getMock();
        $db->expects($this->once())->method("getValue")->willReturn("1562770305");
        $db->expects($this->once())->method("updateValue");
        $this->application->db = $db;
        $obj                   =
            new RSSFetch($this->application, "mock", $this->connection, ["rssUrl" => "https://not.real.com/rss"]);
        $result                = $obj->fetchMessages();
        $this->assertInstanceOf(Messages::class, $result);
        $this->assertEquals(2, count($result));
        $this->assertEquals("Pears", $result[0]->getTitle());
        $this->assertEquals("Bananas", $result[1]->getTitle());
        $this->assertEquals(["https://www.notreal.org/one.jpg", "https://www.notreal.org/two.jpg"],
            $result[1]->getImages());
        $this->assertEquals(["https://www.notreal.org/four.jpg", "https://www.notreal.org/three.jpg"],
            $result[0]->getImages());
    }

    public function testGetMessagesFirstRun()
    {
        $db = $this->getMockBuilder(DB::class)->disableOriginalConstructor()->getMock();
        $db->expects($this->once())->method("getValue")->willReturn("");
        $db->expects($this->once())->method("updateValue");
        $this->application->db = $db;
        $obj                   =
            new RSSFetch($this->application, "mock", $this->connection, ["rssUrl" => "https://not.real.com/rss"]);
        $result                = $obj->fetchMessages();
        $this->assertInstanceOf(Messages::class, $result);
        $this->assertEquals(1, count($result));
        $this->assertEquals("Bananas", $result[0]->getTitle());
        $this->assertEquals(["https://www.notreal.org/one.jpg", "https://www.notreal.org/two.jpg"],
            $result[0]->getImages());
    }

}