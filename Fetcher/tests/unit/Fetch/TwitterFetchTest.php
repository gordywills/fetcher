<?php

use Abraham\TwitterOAuth\TwitterOAuth;
use Fetcher\Application;
use Fetcher\DB\DB;
use Fetcher\Fetch\TwitterFetch;
use Fetcher\Message\Messages;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;

class TwitterFetchTest extends TestCase
{
    private $application;
    private $connection;

    public function setUp(): void
    {
        $this->application         = $this->getMockBuilder(Application::class)->disableOriginalConstructor()->getMock();
        $logger                    =
            $this->getMockBuilder(Logger::class)->disableOriginalConstructor()->getMock();
        $this->application->logger = $logger;
        $this->connection          =
            $this->getMockBuilder(TwitterOAuth::class)->disableOriginalConstructor()->getMock();
    }

    public function testConstructor()
    {
        $obj = new TwitterFetch($this->application, "mock", $this->connection, []);
        $this->assertInstanceOf(TwitterFetch::class, $obj);
    }

    public function testFactoryAllParams()
    {
        $obj = TwitterFetch::factory($this->application, "mock", [], $this->connection);
        $this->assertInstanceOf(TwitterFetch::class, $obj);
    }

    public function testFactoryReqParams()
    {
        $obj = TwitterFetch::factory($this->application, "mock",
            ['consumerKey' => 1, 'consumerSecret' => 1, 'accessToken' => 1, 'accessTokenSecret' => 1]);
        $this->assertInstanceOf(TwitterFetch::class, $obj);
    }

    public function testGetMessagesNormalRoute()
    {
        $this->connection->expects($this->any())->method("get")->willReturnOnConsecutiveCalls((object)[
            "id"   => 10,
            "body" => "other",
        ], [$this->dataObjectOne()]);
        $db = $this->getMockBuilder(DB::class)->disableOriginalConstructor()->getMock();
        $db->expects($this->once())->method("getValue")->willReturn("100");
        $db->expects($this->once())->method("updateValue");
        $this->application->db = $db;
        $obj                   =
            new TwitterFetch($this->application, "mock", $this->connection, ["screenName" => "notReal"]);
        $result                = $obj->fetchMessages();
        $this->assertInstanceOf(Messages::class, $result);
        $this->assertEquals(new DateTimeImmutable("Sun Jun 30 14:41:38 +0000 2019"), $result[0]->getDate());
        $this->assertEquals(["https://pbs.twimg.com/media/D-USHsCXUAA3x-2.jpg"], $result[0]->getImages());
        $this->assertEquals(["https://twitter.com/slingsbyadam/status/1146137893544648704"],
            $result[0]->getAttachments());
    }

    private function dataObjectOne()
    {
        return (object)[
            "created_at"            => "Sun Jun 30 14:41:38 +0000 2019",
            "id"                    => 1145341628321869824,
            "full_text"             => "Thank you to all our amazing Greyshirts who helped spread the #TeamRubicon message this #ArmedForcesDay weekend and thank you to everyone who came to say hi \u263a\ufe0f #Superstars #dreamteam https:\/\/t.co\/vVnaiYhyCC",
            "extended_entities"     => (object)[
                "media" => [
                    (object)[
                        "media_url_https" => "https:\/\/pbs.twimg.com\/media\/D-USHsCXUAA3x-2.jpg",
                    ],
                ],
            ],
            "entities"              => (object)[
                "urls" => [
                    (object)[
                        "expanded_url" => "https:\/\/twitter.com\/slingsbyadam\/status\/1146137893544648704",
                    ],
                ],
            ],
            "in_reply_to_status_id" => null,
            "user"                  => (object)[
                "id" => 10,
            ],
        ];

    }

    public function testGetMessagesFirstRun()
    {
        $this->connection->expects($this->any())->method("get")->willReturnOnConsecutiveCalls((object)[
            "id"   => 10,
            "body" => "other",
        ], [$this->dataObjectOne()]);
        $db = $this->getMockBuilder(DB::class)->disableOriginalConstructor()->getMock();
        $db->expects($this->once())->method("getValue")->willReturn("");
        $db->expects($this->once())->method("updateValue");
        $this->application->db = $db;
        $obj                   =
            new TwitterFetch($this->application, "mock", $this->connection, ["screenName" => "notReal"]);
        $result                = $obj->fetchMessages();
        $this->assertInstanceOf(Messages::class, $result);
        $this->assertEquals(new DateTimeImmutable("Sun Jun 30 14:41:38 +0000 2019"), $result[0]->getDate());
        $this->assertEquals(["https://pbs.twimg.com/media/D-USHsCXUAA3x-2.jpg"], $result[0]->getImages());
    }

    public function testGetMessagesWithMessageFilter()
    {
        $this->connection->expects($this->any())->method("get")->willReturnOnConsecutiveCalls(
            (object)[
                "id"   => 10,
                "body" => "other",
            ],
            $this->dataObjectTwo(),
            $this->dataObjectOne()
        );
        $db = $this->getMockBuilder(DB::class)->disableOriginalConstructor()->getMock();
        $db->expects($this->once())->method("getValue")->willReturn("100");
        $db->expects($this->once())->method("updateValue");
        $this->application->db = $db;
        $obj                   =
            new TwitterFetch($this->application, "mock", $this->connection, ["screenName" => "notReal"]);
        $result                = $obj->fetchMessages();
        $this->assertInstanceOf(Messages::class, $result);
        $this->assertEquals(new DateTimeImmutable("Sun Jun 30 14:41:38 +0000 2019"), $result[0]->getDate());
        $this->assertEquals(["https://pbs.twimg.com/media/D-USHsCXUAA3x-2.jpg"], $result[0]->getImages());
        $this->assertEquals(3, count($result));
    }

    private function dataObjectTwo()
    {
        return [
            (object)[
                "created_at"            => "Sun Jun 30 14:41:38 +0000 2019",
                "id"                    => 1145341628321869824,
                "full_text"             => "Thank you to all our amazing Greyshirts who helped spread the #TeamRubicon message this #ArmedForcesDay weekend and thank you to everyone who came to say hi \u263a\ufe0f #Superstars #dreamteam https:\/\/t.co\/vVnaiYhyCC",
                "extended_entities"     => (object)[
                    "media" => [
                        (object)[
                            "media_url_https" => "https:\/\/pbs.twimg.com\/media\/D-USHsCXUAA3x-2.jpg",
                        ],
                    ],
                ],
                "in_reply_to_status_id" => null,
                "user"                  => (object)[
                    "id" => 10,
                ],
            ],
            (object)[
                "created_at"            => "Sun Jun 30 14:41:38 +0000 2019",
                "id"                    => 1145341628321869825,
                "full_text"             => "Thank you to all our amazing Greyshirts who helped spread the #TeamRubicon message this #ArmedForcesDay weekend and thank you to everyone who came to say hi \u263a\ufe0f #Superstars #dreamteam https:\/\/t.co\/vVnaiYhyCC",
                "extended_entities"     => (object)[
                    "media" => [
                        (object)[
                            "media_url_https" => "https:\/\/pbs.twimg.com\/media\/D-USHsCXUAA3x-2.jpg",
                        ],
                    ],
                ],
                "in_reply_to_status_id" => 1145341628321869824,
                "user"                  => (object)[
                    "id" => 10,
                ],
            ],
            (object)[
                "created_at"            => "Sun Jun 30 14:41:38 +0000 2019",
                "id"                    => 1145341628321869826,
                "full_text"             => "Thank you to all our amazing Greyshirts who helped spread the #TeamRubicon message this #ArmedForcesDay weekend and thank you to everyone who came to say hi \u263a\ufe0f #Superstars #dreamteam https:\/\/t.co\/vVnaiYhyCC",
                "extended_entities"     => (object)[
                    "media" => [
                        (object)[
                            "media_url_https" => "https:\/\/pbs.twimg.com\/media\/D-USHsCXUAA3x-2.jpg",
                        ],
                    ],
                ],
                "in_reply_to_status_id" => 1145341628321869824,
                "user"                  => (object)[
                    "id" => 11,
                ],
            ],
            (object)[
                "created_at"            => "Sun Jun 30 14:41:38 +0000 2019",
                "id"                    => 1145341628321869827,
                "full_text"             => "Thank you to all our amazing Greyshirts who helped spread the #TeamRubicon message this #ArmedForcesDay weekend and thank you to everyone who came to say hi \u263a\ufe0f #Superstars #dreamteam https:\/\/t.co\/vVnaiYhyCC",
                "extended_entities"     => (object)[
                    "media" => [
                        (object)[
                            "media_url_https" => "https:\/\/pbs.twimg.com\/media\/D-USHsCXUAA3x-2.jpg",
                        ],
                    ],
                ],
                "in_reply_to_status_id" => null,
                "user"                  => (object)[
                    "id" => 11,
                ],
            ],
            (object)[
                "created_at"            => "Sun Jun 30 14:41:38 +0000 2019",
                "id"                    => 1145341628321869828,
                "full_text"             => "Thank you to all our amazing Greyshirts who helped spread the #TeamRubicon message this #ArmedForcesDay weekend and thank you to everyone who came to say hi \u263a\ufe0f #Superstars #dreamteam https:\/\/t.co\/vVnaiYhyCC",
                "extended_entities"     => (object)[
                    "media" => [
                        (object)[
                            "media_url_https" => "https:\/\/pbs.twimg.com\/media\/D-USHsCXUAA3x-2.jpg",
                        ],
                    ],
                ],
                "in_reply_to_status_id" => 1145341628321869823,
                "user"                  => (object)[
                    "id" => 10,
                ],
            ],
        ];

    }

}