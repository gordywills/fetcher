<?php

use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;
use Facebook\FacebookResponse;
use Facebook\GraphNodes\GraphEdge;
use Facebook\GraphNodes\GraphNode;
use Fetcher\Application;
use Fetcher\DB\DB;
use Fetcher\Fetch\FacebookFetch;
use Fetcher\Fetch\FetchError;
use Fetcher\Message\Messages;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;
use TestHelpers\IetratorTestHelperTrait;
use TestHelpers\myIterator;

class FacebookFetchTest extends TestCase
{
    use IetratorTestHelperTrait;

    private $application;
    private $connection;

    public function setUp(): void
    {
        $this->application         = $this->getMockBuilder(Application::class)->disableOriginalConstructor()->getMock();
        $logger                    =
            $this->getMockBuilder(Logger::class)->disableOriginalConstructor()->getMock();
        $this->application->logger = $logger;
        $this->connection          =
            $this->getMockBuilder(Facebook::class)->disableOriginalConstructor()->getMock();
    }

    public function testConstructor()
    {
        $obj = new FacebookFetch($this->application, "mock", $this->connection, []);
        $this->assertInstanceOf(FacebookFetch::class, $obj);
    }

    public function testFactoryAllParams()
    {
        $obj = FacebookFetch::factory($this->application, "mock", [], $this->connection);
        $this->assertInstanceOf(FacebookFetch::class, $obj);
    }

    public function testFactoryFailAllParams()
    {
        $this->expectException(FetchError::class);
        FacebookFetch::factory($this->application,
            "mock",
            [
            'app_id'        => null,
            'app_secret'    => null,
            'graph_version' => null,
            'access_token'  => null,
            ], null);

    }
    public function testFactoryReqParams()
    {
        $obj = FacebookFetch::factory($this->application,
            "mock",
            [
                'app_id'        => 1,
                'app_secret'    => 12,
                'graph_version' => 'v2.0',
                'access_token'  => '{access_token}',
            ]);
        $this->assertInstanceOf(FacebookFetch::class, $obj);
    }

    public function testGetMessagesNormalRoute()
    {
        $userNode = $this->getMockBuilder(GraphNode::class)->disableOriginalConstructor()->getMock();
        $userNode->expects($this->exactly(2))->method("getField")->willReturnOnConsecutiveCalls("wendlebury",
            "100");
        $userResponse = $this->getMockBuilder(FacebookResponse::class)->disableOriginalConstructor()->getMock();
        $userResponse->expects($this->once())->method("getGraphNode")->willReturn($userNode);
        $postFromNode = $this->getMockBuilder(GraphNode::class)->disableOriginalConstructor()->getMock();
        $postFromNode->expects($this->exactly(2))->method("getField")->willReturnOnConsecutiveCalls("100", "200");

        $postNode = $this->getMockBuilder(GraphNode::class)->disableOriginalConstructor()->getMock();
        $postNode->expects($this->any())->method("getField")->willReturnOnConsecutiveCalls(
            $postFromNode,
            $postFromNode,
            "200",
            "100",
            "https://not.a.real.url/for/a/picture.png",
            new DateTime("Sun Jun 30 14:41:38 +0000 2019"),
            "message body",
            "https://not.a.real.url/for/a/picture2.png",
            "https://not.a.real.url/for/a/picture2.png",
            "100",
            "https://not.a.real.url/for/a/picture.png",
            new DateTime("Sun Jun 30 14:41:38 +0000 2019"),
            "message body",
            "https://not.a.real.url/for/a/picture2.png",
            "https://not.a.real.url/for/a/picture2.png"
        );
        $postNode->expects($this->exactly(2))->method("asJson")->willReturn('{"some":"valid","json":"values"}');

        $postEdgeIterator = $this->getMockBuilder(myIterator::class)->disableOriginalConstructor()->getMock();
        $postEdge         = $this->getMockBuilder(GraphEdge::class)->disableOriginalConstructor()->getMock();

        $postEdge->expects($this->any())->method("getIterator")->willReturn($this->mockIterator($postEdgeIterator,
            [$postNode, $postNode]));
        $postsResponse =
            $this->getMockBuilder(FacebookResponse::class)->disableOriginalConstructor()->onlyMethods(["getGraphEdge"])->getMock();
        $postsResponse->expects($this->once())->method("getGraphEdge")->willReturn($postEdge);
        $this->connection->expects($this->any())->method("get")->willReturnOnConsecutiveCalls(
            $userResponse,
            $postsResponse
        );
        $db = $this->getMockBuilder(DB::class)->disableOriginalConstructor()->getMock();
        $db->expects($this->once())->method("getValue")->willReturn("100");
        $db->expects($this->once())->method("updateValue");
        $this->application->db = $db;

        $obj    = new FacebookFetch($this->application, "mock", $this->connection, [
            'app_id'        => 1,
            'app_secret'    => 12,
            'graph_version' => 'v2.0',
            'access_token'  => '{access_token}',
        ]);
        $result = $obj->fetchMessages();
        $this->assertInstanceOf(Messages::class, $result);
        $this->assertEquals(new DateTimeImmutable("Sun Jun 30 14:41:38 +0000 2019"), $result[0]->getDate());
        $this->assertEquals(["https://not.a.real.url/for/a/picture2.png"], $result[0]->getImages());
        $this->assertEquals([], $result[0]->getAttachments());
        $this->assertEquals("message body", $result[0]->getBody());
    }

    public function testGetMessagesFirstRun()
    {
        $userNode = $this->getMockBuilder(GraphNode::class)->disableOriginalConstructor()->getMock();
        $userNode->expects($this->exactly(2))->method("getField")->willReturnOnConsecutiveCalls("wendlebury",
            "100");
        $userResponse = $this->getMockBuilder(FacebookResponse::class)->disableOriginalConstructor()->getMock();
        $userResponse->expects($this->once())->method("getGraphNode")->willReturn($userNode);
        $postFromNode = $this->getMockBuilder(GraphNode::class)->disableOriginalConstructor()->getMock();
        $postFromNode->expects($this->exactly(2))->method("getField")->willReturnOnConsecutiveCalls("100", "200");

        $postNode = $this->getMockBuilder(GraphNode::class)->disableOriginalConstructor()->getMock();
        $postNode->expects($this->any())->method("getField")->willReturnOnConsecutiveCalls(
            $postFromNode,
            $postFromNode,
            "200",
            "100",
            "https://not.a.real.url/for/a/picture.png",
            new DateTime("Sun Jun 30 14:41:38 +0000 2019"),
            "message body",
            "https://not.a.real.url/for/a/picture2.png",
            "https://not.a.real.url/for/a/picture2.png",
            "100",
            "https://not.a.real.url/for/a/picture.png",
            new DateTime("Sun Jun 30 14:41:38 +0000 2019"),
            "message body",
            "https://not.a.real.url/for/a/picture2.png",
            "https://not.a.real.url/for/a/picture2.png"
        );
        $postNode->expects($this->exactly(2))->method("asJson")->willReturn('{"some":"valid","json":"values"}');

        $postEdgeIterator = $this->getMockBuilder(myIterator::class)->disableOriginalConstructor()->getMock();
        $postEdge         = $this->getMockBuilder(GraphEdge::class)->disableOriginalConstructor()->getMock();

        $postEdge->expects($this->any())->method("getIterator")->willReturn($this->mockIterator($postEdgeIterator,
            [$postNode, $postNode]));
        $postsResponse =
            $this->getMockBuilder(FacebookResponse::class)->disableOriginalConstructor()->onlyMethods(["getGraphEdge"])->getMock();
        $postsResponse->expects($this->once())->method("getGraphEdge")->willReturn($postEdge);
        $this->connection->expects($this->any())->method("get")->willReturnOnConsecutiveCalls(
            $userResponse,
            $postsResponse
        );
        $db = $this->getMockBuilder(DB::class)->disableOriginalConstructor()->getMock();
        $db->expects($this->once())->method("getValue")->willReturn("");
        $db->expects($this->once())->method("updateValue");
        $this->application->db = $db;

        $obj    = new FacebookFetch($this->application, "mock", $this->connection, [
            'app_id'        => 1,
            'app_secret'    => 12,
            'graph_version' => 'v2.0',
            'access_token'  => '{access_token}',
        ]);
        $result = $obj->fetchMessages();
        $this->assertInstanceOf(Messages::class, $result);
        $this->assertEquals(new DateTimeImmutable("Sun Jun 30 14:41:38 +0000 2019"), $result[0]->getDate());
        $this->assertEquals(["https://not.a.real.url/for/a/picture2.png"], $result[0]->getImages());
        $this->assertEquals([], $result[0]->getAttachments());
        $this->assertEquals("message body", $result[0]->getBody());
    }

    public function testUserGetFail()
    {
        $userResponse = $this->getMockBuilder(FacebookResponse::class)->disableOriginalConstructor()->getMock();
        $userResponse->expects($this->once())->method("getGraphNode")->willThrowException(new FacebookSDKException("boom"));
        $this->connection->expects($this->any())->method("get")->willReturnOnConsecutiveCalls(
            $userResponse
        );
        $db                    = $this->getMockBuilder(DB::class)->disableOriginalConstructor()->getMock();
        $this->application->db = $db;

        $obj = new FacebookFetch($this->application, "mock", $this->connection, [
            'app_id'        => 1,
            'app_secret'    => 12,
            'graph_version' => 'v2.0',
            'access_token'  => '{access_token}',
        ]);
        $this->expectException(FetchError::class);
        $obj->fetchMessages();
    }

    public function testPostGetFail()
    {
        $userNode = $this->getMockBuilder(GraphNode::class)->disableOriginalConstructor()->getMock();
        $userNode->expects($this->exactly(2))->method("getField")->willReturnOnConsecutiveCalls("wendlebury",
            "100");
        $userResponse = $this->getMockBuilder(FacebookResponse::class)->disableOriginalConstructor()->getMock();
        $userResponse->expects($this->once())->method("getGraphNode")->willReturn($userNode);
        $this->connection->expects($this->any())->method("get")->willReturnOnConsecutiveCalls(
            $this->returnValue($userResponse),
            $this->throwException(new FacebookSDKException("boom"))
        );
        $db                    = $this->getMockBuilder(DB::class)->disableOriginalConstructor()->getMock();
        $this->application->db = $db;

        $obj = new FacebookFetch($this->application, "mock", $this->connection, [
            'app_id'        => 1,
            'app_secret'    => 12,
            'graph_version' => 'v2.0',
            'access_token'  => '{access_token}',
        ]);
        $this->expectException(FetchError::class);
        $obj->fetchMessages();
    }

    public function testUserGetNodeFail()
    {
        $this->connection->expects($this->any())->method("get")->willThrowException(new FacebookSDKException("boon"));
        $db                    = $this->getMockBuilder(DB::class)->disableOriginalConstructor()->getMock();
        $this->application->db = $db;

        $obj = new FacebookFetch($this->application, "mock", $this->connection, [
            'app_id'        => 1,
            'app_secret'    => 12,
            'graph_version' => 'v2.0',
            'access_token'  => '{access_token}',
        ]);
        $this->expectException(FetchError::class);
        $obj->fetchMessages();
    }

    public function testPostGetEdgeFail()
    {
        $userNode = $this->getMockBuilder(GraphNode::class)->disableOriginalConstructor()->getMock();
        $userNode->expects($this->exactly(2))->method("getField")->willReturnOnConsecutiveCalls("wendlebury",
            "100");
        $userResponse = $this->getMockBuilder(FacebookResponse::class)->disableOriginalConstructor()->getMock();
        $userResponse->expects($this->once())->method("getGraphNode")->willReturn($userNode);
        $postsResponse =
            $this->getMockBuilder(FacebookResponse::class)->disableOriginalConstructor()->onlyMethods(["getGraphEdge"])->getMock();
        $postsResponse->expects($this->once())->method("getGraphEdge")->willThrowException(new FacebookSDKException("boom"));
        $this->connection->expects($this->any())->method("get")->willReturnOnConsecutiveCalls(
            $this->returnValue($userResponse),
            $this->returnValue($postsResponse)
        );
        $db                    = $this->getMockBuilder(DB::class)->disableOriginalConstructor()->getMock();
        $this->application->db = $db;

        $obj = new FacebookFetch($this->application, "mock", $this->connection, [
            'app_id'        => 1,
            'app_secret'    => 12,
            'graph_version' => 'v2.0',
            'access_token'  => '{access_token}',
        ]);
        $this->expectException(FetchError::class);
        $obj->fetchMessages();
    }

}