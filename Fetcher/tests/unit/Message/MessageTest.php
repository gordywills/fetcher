<?php

use Fetcher\Application;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;

class MessageTest extends TestCase
{
    private $testMessage;
    private $application;

    public function setUp(): void
    {
        $this->application         = $this->getMockBuilder(Application::class)->disableOriginalConstructor()->getMock();
        $logger                    =
            $this->getMockBuilder(Logger::class)->disableOriginalConstructor()->getMock();
        $this->application->logger = $logger;
        $this->testMessage         = new Fetcher\Message\Message($this->application, '{
  "created_at": "Mon Jun 03 18:59:29 +0000 2019",
  "id": 1135622045767786497,
  "id_str": "1135622045767786497",
  "full_text": "The important part for me is spending time with the wonderful mix of #volunteers in the organisation, learning from them, making new friends &amp; building a level of trust you don\'t often reach so when the time comes you know you can rely on the team.\' #VolunteersWeek https:\/\/t.co\/pvL0kyUsvF",
  "truncated": false}');
    }

    public function testTitleGetterSetter()
    {
        $this->testMessage->setTitle("A Title");
        $this->assertEquals("A Title", $this->testMessage->getTitle());
    }

    public function testAttachmentsGetterSetter()
    {
        $this->testMessage->setAttachments(["http://not.real/doc.pdf", "https://also.not.real/doc.md"]);
        $this->assertEquals(["http://not.real/doc.pdf", "https://also.not.real/doc.md"],
            $this->testMessage->getAttachments());
    }

    public function testBodyGetterSetter()
    {
        $this->testMessage->setBody("Some Body Text");
        $this->assertEquals("Some Body Text", $this->testMessage->getBody());
    }

    public function testBodyGetterSetterNullBody()
    {
        $this->testMessage->setBody(null);
        $this->assertEquals("", $this->testMessage->getBody());
    }

    public function testDateGetterSetter()
    {
        $now = $this->getMockBuilder(DateTimeImmutable::class)->disableOriginalConstructor()->getMock();
        $this->testMessage->setDate($now);
        $this->assertSame($now, $this->testMessage->getDate());
    }

    public function testImagesGetterSetter()
    {
        $this->testMessage->setImages(["http://not.real/doc.png", "https://also.not.real/doc.gif"]);
        $this->assertEquals(["http://not.real/doc.png", "https://also.not.real/doc.gif"],
            $this->testMessage->getImages());
    }

    public function testRawMessageGetterSetter()
    {
        $this->testMessage->setRawMessage('{"1": "2"}');
        $this->assertEquals('{"1": "2"}', $this->testMessage->getRawMessage());
    }

    public function testConstructor()
    {
        $this->assertEquals('{
  "created_at": "Mon Jun 03 18:59:29 +0000 2019",
  "id": 1135622045767786497,
  "id_str": "1135622045767786497",
  "full_text": "The important part for me is spending time with the wonderful mix of #volunteers in the organisation, learning from them, making new friends &amp; building a level of trust you don\'t often reach so when the time comes you know you can rely on the team.\' #VolunteersWeek https://t.co/pvL0kyUsvF",
  "truncated": false}', $this->testMessage->getRawMessage());
    }

    public function testSourceGetterSetter()
    {
        $this->testMessage->setSource('twitter', "bulldogUK");
        $this->assertEquals(["service" => "twitter", "user" => "bulldogUK"], $this->testMessage->getSource());
    }

    public function testIDGetterSetter()
    {
        $this->testMessage->setId(10);
        $this->assertEquals(10, $this->testMessage->getId());
    }

    public function testUrlGetterSetter()
    {
        $this->testMessage->setUrl("https://not.real.com/slug/parts/go/here?query=params");
        $this->assertEquals("https://not.real.com/slug/parts/go/here?query=params", $this->testMessage->getUrl());
    }
}