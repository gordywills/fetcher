<?php

use Fetcher\Message\Message;
use Fetcher\Message\Messages;
use PHPUnit\Framework\TestCase;

class MessagesTest extends TestCase
{
    private $testMessages;
    private $mockValue;

    public function setUp(): void
    {
        $this->testMessages = new Messages();
        $this->mockValue    = $this->getMockBuilder(Message::class)->disableOriginalConstructor()->getMock();
        $this->testMessages->offsetSet(0, $this->mockValue);
        $this->testMessages->offsetSet(1, $this->mockValue);
        $this->testMessages->offsetSet(2, $this->mockValue);
    }

    public function testImplements()
    {
        $this->assertInstanceOf(ArrayAccess::class, $this->testMessages);
        $this->assertInstanceOf(Countable::class, $this->testMessages);
        $this->assertInstanceOf(Iterator::class, $this->testMessages);
    }

    public function testCompare()
    {
        $dateEarly     = $this->getMockBuilder(Message::class)->disableOriginalConstructor()->getMock();
        $dateEarlyDate = $this->getMockBuilder(DateTimeImmutable::class)->setConstructorArgs(['yesterday'])->getMock();
        $dateLate      = $this->getMockBuilder(Message::class)->disableOriginalConstructor()->getMock();
        $dateLateDate  = $this->getMockBuilder(DateTimeImmutable::class)->setConstructorArgs(['today'])->getMock();
        $dateEarly->expects($this->any())->method('getDate')->will($this->returnValue($dateEarlyDate));
        $dateLate->expects($this->any())->method('getDate')->will($this->returnValue($dateLateDate));

        $this->assertEquals(0, Messages::cmp($dateEarly, $dateEarly));
        $this->assertEquals(-1, Messages::cmp($dateEarly, $dateLate));
        $this->assertEquals(1, Messages::cmp($dateLate, $dateEarly));
    }

    public function testOffsetSetBadValue()
    {
        $badValue = $this->getMockBuilder(SomeClass::class)->disableOriginalConstructor()->getMock();
        $this->expectException(TypeError::class);
        $this->testMessages->offsetSet("key", $badValue);
    }

    public function testOffsetSetGet()
    {
        $value2                = $this->getMockBuilder(Message::class)->disableOriginalConstructor()->getMock();
        $this->testMessages[1] = $value2;
        $this->testMessages[]  = $this->mockValue;
        $this->testMessages[]  = $value2;
        $this->assertSame($this->mockValue, $this->testMessages[0]);
        $this->assertSame($value2, $this->testMessages[1]);
        $this->assertSame($this->mockValue, $this->testMessages[2]);
        $this->assertSame($this->mockValue, $this->testMessages[3]);
        $this->assertSame($value2, $this->testMessages[4]);
    }

    public function testOffsetExists()
    {

        $this->assertTrue($this->testMessages->offsetExists(1));
        $this->assertTrue(isset($this->testMessages[1]));
        $this->assertFalse($this->testMessages->offsetExists(5));
        $this->assertFalse(isset($this->testMessages["notKey"]));
    }

    public function testOffsetUnset()
    {
        $this->assertTrue(isset($this->testMessages[1]));
        unset($this->testMessages[1]);
        $this->assertFalse(isset($this->testMessages[1]));

    }

    public function testCount()
    {
        $this->assertEquals(3, count($this->testMessages));
        $this->testMessages[] = $this->mockValue;
        $this->testMessages[] = $this->mockValue;
        $this->assertEquals(5, count($this->testMessages));
    }

    public function testCurrentKeyNextRewind()
    {
        $value2                = $this->getMockBuilder(Message::class)->disableOriginalConstructor()->getMock();
        $this->testMessages[1] = $value2;
        $this->testMessages->rewind();
        $this->assertEquals(0, $this->testMessages->key());
        $this->assertSame($this->mockValue, $this->testMessages->current());
        $this->testMessages->next();
        $this->assertEquals(1, $this->testMessages->key());
        $this->assertSame($value2, $this->testMessages->current());
        $this->testMessages->next();
        $this->assertEquals(2, $this->testMessages->key());
        $this->assertSame($this->mockValue, $this->testMessages->current());
        $this->testMessages->rewind();
        $this->assertEquals(0, $this->testMessages->key());
        $this->assertSame($this->mockValue, $this->testMessages->current());
    }

    public function testValid()
    {
        $this->testMessages->rewind();
        $this->assertTrue($this->testMessages->valid());
        $this->testMessages->next();
        $this->testMessages->next();
        $this->testMessages->next();
        $this->assertFalse($this->testMessages->valid());
    }

    public function testSortMessages()
    {
        $dateEarly     = $this->getMockBuilder(Message::class)->disableOriginalConstructor()->getMock();
        $dateEarlyDate = $this->getMockBuilder(DateTimeImmutable::class)->setConstructorArgs(['yesterday'])->getMock();
        $dateLate      = $this->getMockBuilder(Message::class)->disableOriginalConstructor()->getMock();
        $dateLateDate  = $this->getMockBuilder(DateTimeImmutable::class)->setConstructorArgs(['today'])->getMock();
        $dateLater     = $this->getMockBuilder(Message::class)->disableOriginalConstructor()->getMock();
        $dateLaterDate = $this->getMockBuilder(DateTimeImmutable::class)->setConstructorArgs(['tomorrow'])->getMock();
        $dateEarly->expects($this->any())->method('getDate')->will($this->returnValue($dateEarlyDate));
        $dateLate->expects($this->any())->method('getDate')->will($this->returnValue($dateLateDate));
        $dateLater->expects($this->any())->method('getDate')->will($this->returnValue($dateLaterDate));
        $this->testMessages[0] = $dateLate;
        $this->testMessages[1] = $dateEarly;
        $this->testMessages[2] = $dateLater;
        $this->testMessages    = $this->testMessages->sortMessages();
        $this->assertSame($dateEarly, $this->testMessages[0]);
        $this->assertSame($dateLate, $this->testMessages[1]);
        $this->assertSame($dateLater, $this->testMessages[2]);
    }
}